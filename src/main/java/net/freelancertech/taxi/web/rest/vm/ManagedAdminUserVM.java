package net.freelancertech.taxi.web.rest.vm;

import net.freelancertech.taxi.api.typage.AuthoritiyAdminRoleEnum;
import net.freelancertech.taxi.service.dto.UserDTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedAdminUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 100;
    @NotNull
    private AuthoritiyAdminRoleEnum adminRole;


    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;



    public String getPassword() {
        return password;
    }

    public AuthoritiyAdminRoleEnum getAdminRole() {
        return adminRole;
    }

    public void setAdminRole(AuthoritiyAdminRoleEnum adminRole) {
        this.adminRole = adminRole;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ManagedAdminUserVM login(String login) {
        this.login = login;
        return this;
    }

    public ManagedAdminUserVM firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public ManagedAdminUserVM lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public ManagedAdminUserVM email(String email) {
        this.email = email;
        return this;
    }

    public ManagedAdminUserVM activated(boolean activated) {
        this.activated = activated;
        return this;
    }

    public ManagedAdminUserVM langKey(String langKey) {
        this.langKey = langKey;
        return this;
    }

    public ManagedAdminUserVM authorities(Set<String> authorities) {
        this.authorities = authorities;
        return this;
    }

    public ManagedAdminUserVM organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public ManagedAdminUserVM telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public ManagedAdminUserVM password(String password) {
        this.password = password;
        return this;
    }
}
