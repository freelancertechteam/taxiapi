package net.freelancertech.taxi.web.rest.vm;

import net.freelancertech.taxi.service.dto.UserDTO;

import javax.validation.constraints.Size;
import java.util.Set;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 100;


    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    public ManagedUserVM() {
    }


    public ManagedUserVM(String login, String password, String firstName, String lastName,
                         String email, boolean activated, String langKey, Set<String> authorities) {
        super(login, firstName, lastName, email, activated, langKey, authorities);

        this.password = password;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ManagedUserVM login(String login) {
        this.login = login;
        return this;
    }

    public ManagedUserVM firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public ManagedUserVM lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public ManagedUserVM email(String email) {
        this.email = email;
        return this;
    }

    public ManagedUserVM activated(boolean activated) {
        this.activated = activated;
        return this;
    }

    public ManagedUserVM langKey(String langKey) {
        this.langKey = langKey;
        return this;
    }

    public ManagedUserVM authorities(Set<String> authorities) {
        this.authorities = authorities;
        return this;
    }

    public ManagedUserVM organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public ManagedUserVM telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public ManagedUserVM password(String password) {
        this.password = password;
        return this;
    }
}
