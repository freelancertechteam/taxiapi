package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;


/**
 * A DTO for the Conduire entity.
 */
public class ConduireDTO implements Serializable {


    @NotNull
    private Boolean tutilaire;

    @NotNull
    private LocalDate dateDebut;

    private LocalDate dateFin;

    @NotNull
    private String raisonRetrait;




    private Long taxiDriverId;

    private Long vehiculeId;
    public Boolean getTutilaire() {
        return tutilaire;
    }

    public void setTutilaire(Boolean tutilaire) {
        this.tutilaire = tutilaire;
    }
    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }
    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }
    public String getRaisonRetrait() {
        return raisonRetrait;
    }

    public void setRaisonRetrait(String raisonRetrait) {
        this.raisonRetrait = raisonRetrait;
    }

    
    public Long getTaxiDriverId() {
        return taxiDriverId;
    }

    public void setTaxiDriverId(Long taxiDriverId) {
        this.taxiDriverId = taxiDriverId;
    }

    public Long getVehiculeId() {
        return vehiculeId;
    }

    public void setVehiculeId(Long vehiculeId) {
        this.vehiculeId = vehiculeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConduireDTO)) return false;
        ConduireDTO that = (ConduireDTO) o;
        return Objects.equals(getTaxiDriverId(), that.getTaxiDriverId()) &&
            Objects.equals(getVehiculeId(), that.getVehiculeId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTaxiDriverId(), getVehiculeId());
    }

    @Override
    public String toString() {
        return "ConduireDTO{" +
            ", tutilaire='" + tutilaire + "'" +
            ", dateDebut='" + dateDebut + "'" +
            ", dateFin='" + dateFin + "'" +
            ", raisonRetrait='" + raisonRetrait + "'" +
            '}';
    }
}
