package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.service.dto.enumeration.LikeEnum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A DTO for the Like entity.
 */
public class LikeParam implements Serializable {


    @NotNull
    @Size(min = 1, max = 60)
    private String name;

    @Size(max = 30)
    private String email;

    @NotNull
    @Size(min = 1, max = 30)
    private String telephone;

    @NotNull
    private LikeEnum valeur;



    private String taxiDriverCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public LikeEnum getValeur() {
        return valeur;
    }

    public void setValeur(LikeEnum valeur) {
        this.valeur = valeur;
    }



    public String getTaxiDriverCode() {
        return taxiDriverCode;
    }

    public void setTaxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
    }


    public LikeParam name(String name) {
        this.name = name;
        return this;
    }

    public LikeParam email(String email) {
        this.email = email;
        return this;
    }

    public LikeParam telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public LikeParam valeur(LikeEnum valeur) {
        this.valeur = valeur;
        return this;
    }

    public LikeParam taxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
        return this;
    }
}
