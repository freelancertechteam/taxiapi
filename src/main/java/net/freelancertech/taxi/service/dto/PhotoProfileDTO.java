package net.freelancertech.taxi.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Photo.
 */
public class PhotoProfileDTO implements  Serializable {

    private static final long serialVersionUID = 1L;


    private String description;

    private byte[] valeur;

    private String valeurContentType;

    private String userLogin;

    private Integer version;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getValeur() {
        return valeur;
    }

    public void setValeur(byte[] valeur) {
        this.valeur = valeur;
    }

    public String getValeurContentType() {
        return valeurContentType;
    }

    public void setValeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhotoProfileDTO)) return false;
        PhotoProfileDTO that = (PhotoProfileDTO) o;
        return Objects.equals(getUserLogin(), that.getUserLogin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserLogin());
    }

    public PhotoProfileDTO description(String description) {
        this.description = description;
        return this;
    }

    public PhotoProfileDTO valeur(byte[] valeur) {
        this.valeur = valeur;
        return this;
    }

    public PhotoProfileDTO valeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
        return this;
    }

    public PhotoProfileDTO userLogin(String userLogin) {
        this.userLogin = userLogin;
        return this;
    }

    public PhotoProfileDTO version(Integer version) {
        this.version = version;
        return this;
    }

}
