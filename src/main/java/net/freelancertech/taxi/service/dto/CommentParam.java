package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


/**
 * A DTO for the Comment entity.
 */
public class CommentParam implements Serializable {

    @NotNull
    @Size(min = 1, max = 30)
    private String name;

    @Size(max = 30)
    private String email;

    @NotNull
    @Size(min = 1, max = 30)
    private String telephone;

    @NotNull
    @Size(min = 1, max = 255)
    private String value;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public CommentParam name(String name) {
        this.name = name;
        return this;
    }

    public CommentParam email(String email) {
        this.email = email;
        return this;
    }

    public CommentParam telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public CommentParam value(String value) {
        this.value = value;
        return this;
    }
}
