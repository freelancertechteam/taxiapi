package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;


/**
 * A DTO for the PassagerCoordonnee entity.
 */
public class PassagerCoordonneeDTO implements Serializable {


    @NotNull
    private BigDecimal latitude;

    @NotNull
    private BigDecimal longitude;

    private String passagerCode;

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getPassagerCode() {
        return passagerCode;
    }

    public void setPassagerCode(String passagerCode) {
        this.passagerCode = passagerCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PassagerCoordonneeDTO)) return false;
        PassagerCoordonneeDTO that = (PassagerCoordonneeDTO) o;
        return Objects.equals(getLatitude(), that.getLatitude()) &&
                Objects.equals(getLongitude(), that.getLongitude()) &&
                Objects.equals(getPassagerCode(), that.getPassagerCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLatitude(), getLongitude(), getPassagerCode());
    }

    @Override
    public String toString() {
        return "PassagerCoordonneeDTO{" +
            ", latitude='" + latitude + "'" +
            ", longitude='" + longitude + "'" +
            '}';
    }


}
