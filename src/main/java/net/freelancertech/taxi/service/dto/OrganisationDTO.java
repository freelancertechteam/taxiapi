package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.api.OrganisationEnum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Organisation entity.
 */
public class OrganisationDTO implements Serializable {


    @NotNull
    @Size(max = 10)
    private String code;

    @NotNull
    private String designation;

    @Size(min = 0)
    private String description;

    @NotNull
    @Size(max = 155)
    private String abreviation;

    @NotNull
    private Integer version;

    private byte[] logo;

    private String logoContentType;
    private OrganisationEnum nature;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public OrganisationEnum getNature() {
        return nature;
    }

    public void setNature(OrganisationEnum nature) {
        this.nature = nature;
    }

    public OrganisationDTO nature(OrganisationEnum nature) {
        this.nature = nature;
        return this;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrganisationDTO)) return false;
        OrganisationDTO that = (OrganisationDTO) o;
        return Objects.equals(getCode(), that.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }

    @Override
    public String toString() {
        return "OrganisationDTO{" +
            ", code='" + code + "'" +
            ", designation='" + designation + "'" +
            ", description='" + description + "'" +
            ", abreviation='" + abreviation + "'" +
            ", version='" + version + "'" +
            ", logo='" + logo + "'" +
            '}';
    }

    public OrganisationDTO code(String code) {
        this.code = code;
        return this;
    }

    public OrganisationDTO designation(String designation) {
        this.designation = designation;
        return this;
    }

    public OrganisationDTO description(String description) {
        this.description = description;
        return this;
    }

    public OrganisationDTO abreviation(String abreviation) {
        this.abreviation = abreviation;
        return this;
    }

    public OrganisationDTO version(Integer version) {
        this.version = version;
        return this;
    }

    public OrganisationDTO logo(byte[] logo) {
        this.logo = logo;
        return this;
    }

    public OrganisationDTO logoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
        return this;
    }
}
