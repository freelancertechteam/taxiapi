package net.freelancertech.taxi.service.dto;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by simon on 08/03/2017.
 */
public class EnveloppeSms {
    private static final SimpleDateFormat DATE_FORMAT=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private final Map<String,Object> map=new HashMap<>();

    public EnveloppeSms(){
        map.put("SMS",new Sms());
        map.put("LOWCOST","1");
    }
    public EnveloppeSms addMessage(String message){
        map.put("MESSAGE",message);
        return this;
    }
    public EnveloppeSms addCampaignName(String campaignName){
        map.put("CAMPAIGN_NAME",campaignName);
        return this;
    }
    public EnveloppeSms addEmetteur(String emetteur){
        map.put("TPOA",emetteur);
        return this;
    }
    public EnveloppeSms addDynamic(String dynamic){
        map.put("DYNAMIC",dynamic);
        return this;
    }
    public EnveloppeSms addLowCost(String lowCost){
        map.put("LOWCOST",lowCost);
        return this;
    }
    public EnveloppeSms addDate(Date date){
        map.put("DATE",DATE_FORMAT.format(date));
        return this;
    }
    public EnveloppeSms addTelephone(String telephone){
        Map<String,String> phone=new HashMap<>();
        phone.put("MOBILEPHONE",telephone);
        ((Sms) map.get("SMS")).getList()
        .add(phone);
        return this;
    }


    @JsonGetter("DATA")
    public Map<String, Object> getMap() {
        return map;
    }

    static class Sms{
        private final List<Map<String,String>> list=new ArrayList<>();

        public List<Map<String, String>> getList() {
            return list;
        }
    }
}
