package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Capture entity.
 */
public class CaptureDTO implements Serializable {

    @NotNull
    @Size(max = 10)
    private String code;

    @NotNull

    private byte[] valeur;

    private String valeurContentType;

    private Integer version;



    private Long attachmentId;
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public byte[] getValeur() {
        return valeur;
    }

    public void setValeur(byte[] valeur) {
        this.valeur = valeur;
    }

    public String getValeurContentType() {
        return valeurContentType;
    }

    public void setValeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
    }


    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CaptureDTO)) return false;
        CaptureDTO that = (CaptureDTO) o;
        return Objects.equals(getCode(), that.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }

    @Override
    public String toString() {
        return "CaptureDTO{" +
            ", code='" + code + "'" +
            ", valeur='" + valeur + "'" +
            '}';
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
