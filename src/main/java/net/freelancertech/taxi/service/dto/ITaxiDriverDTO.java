package net.freelancertech.taxi.service.dto;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by simon on 21/01/2017.
 */
public interface ITaxiDriverDTO extends Serializable {
    String getCode();

    String getLastName();

    LocalDate getBirthDate();

    String getPlaceOfBirth();

    String getTelephoneNumber();

    String getNicn();

    String getDrivingLicenseNumber();

    String getCapacityNumber();
}
