package net.freelancertech.taxi.service.dto;

import java.util.Objects;

/**
 * Created by simon on 23/01/2017.
 */
public class ParametreOrganisationDTO {
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ParametreOrganisationDTO key(String key) {
        this.key = key;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParametreOrganisationDTO)) return false;
        ParametreOrganisationDTO that = (ParametreOrganisationDTO) o;
        return Objects.equals(getKey(), that.getKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKey());
    }
}
