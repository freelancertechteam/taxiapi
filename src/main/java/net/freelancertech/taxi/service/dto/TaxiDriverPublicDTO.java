package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.api.typage.SituationEnum;
import net.freelancertech.taxi.api.typage.SituationMatrimonialeEnum;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;


/**
 * A DTO for the TaxiDriver entity.
 */
public class TaxiDriverPublicDTO implements Serializable {

    private String code;

    @Size(max = 60)
    private String firstName;

    @NotNull
    private String lastName;

    private byte[] valeur;

    private String valeurContentType;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public byte[] getValeur() {
        return valeur;
    }

    public void setValeur(byte[] valeur) {
        this.valeur = valeur;
    }

    public String getValeurContentType() {
        return valeurContentType;
    }

    public void setValeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
    }

    public TaxiDriverPublicDTO code(String code) {
        this.code = code;
        return this;
    }

    public TaxiDriverPublicDTO firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TaxiDriverPublicDTO lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TaxiDriverPublicDTO valeur(byte[] valeur) {
        this.valeur = valeur;
        return this;
    }

    public TaxiDriverPublicDTO valeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaxiDriverPublicDTO)) return false;
        TaxiDriverPublicDTO that = (TaxiDriverPublicDTO) o;
        return Objects.equals(getCode(), that.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }
}
