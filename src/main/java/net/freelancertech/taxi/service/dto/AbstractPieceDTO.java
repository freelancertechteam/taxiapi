package net.freelancertech.taxi.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A Photo.
 */

public abstract class AbstractPieceDTO implements  Serializable {

    private static final long serialVersionUID = 1L;


    protected String description;


    protected byte[] valeur;


    protected String valeurContentType;


    protected String taxiDriverCode;

    protected Integer version;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getValeur() {
        return valeur;
    }

    public void setValeur(byte[] valeur) {
        this.valeur = valeur;
    }

    public String getValeurContentType() {
        return valeurContentType;
    }

    public void setValeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
    }

    public String getTaxiDriverCode() {
        return taxiDriverCode;
    }

    public void setTaxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public AbstractPieceDTO description(String description) {
        this.description = description;
        return this;
    }

    public AbstractPieceDTO valeur(byte[] valeur) {
        this.valeur = valeur;
        return this;
    }

    public AbstractPieceDTO valeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
        return this;
    }

    public AbstractPieceDTO taxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
        return this;
    }

    public AbstractPieceDTO version(Integer version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractPieceDTO)) return false;
        AbstractPieceDTO that = (AbstractPieceDTO) o;
        return Objects.equals(getTaxiDriverCode(), that.getTaxiDriverCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTaxiDriverCode());
    }
}
