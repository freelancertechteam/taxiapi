package net.freelancertech.taxi.service.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by SimonPascal on 08/08/2015.
 */
@Data
public class EntityToDeleteDTO {
    @NotNull
    private String code;
    @NotNull
    private Integer version;


    public EntityToDeleteDTO code(String code){
        this.code=code;
        return this;
    }
    public EntityToDeleteDTO version(Integer version){
        this.version=version;
        return this;
    }

    
}
