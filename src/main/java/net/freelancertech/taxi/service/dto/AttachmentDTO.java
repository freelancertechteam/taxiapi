package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Attachment entity.
 */
public class AttachmentDTO implements Serializable {


    @NotNull
    @Size(max = 60)
    private String number;

    @NotNull
    private String front;

    private String back;

    private String comment;
    private Integer version;

    private Long taxiDriverId;
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public String getFront() {
        return front;
    }

    public void setFront(String front) {
        this.front = front;
    }
    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public Long getTaxiDriverId() {
        return taxiDriverId;
    }

    public void setTaxiDriverId(Long taxiDriverId) {
        this.taxiDriverId = taxiDriverId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AttachmentDTO attachmentDTO = (AttachmentDTO) o;

        if ( ! Objects.equals(number, attachmentDTO.number)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(number);
    }

    @Override
    public String toString() {
        return "AttachmentDTO{" +
            ", number='" + number + "'" +
            ", front='" + front + "'" +
            ", back='" + back + "'" +
            ", comment='" + comment + "'" +
            '}';
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
