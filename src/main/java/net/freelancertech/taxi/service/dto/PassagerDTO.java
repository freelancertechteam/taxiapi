package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.api.typage.PassagerEnum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;


/**
 * A DTO for the Passager entity.
 */
public class PassagerDTO implements Serializable {


    @NotNull
    @Size(max = 35)
    private String phone;

    @Size(max = 150)
    private String code;

    private PassagerEnum passagerType;

    @NotNull
    private String nomPrenom;

    @NotNull
    private String lieuArrive;

    private String lieuDepart;

    private BigDecimal latitude;

    private BigDecimal longitude;

    private String divers;

    private String phoneImei;

    private String phoneSerialNumber;
    private Integer version;


    private String taxiDriverCode;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getNomPrenom() {
        return nomPrenom;
    }

    public void setNomPrenom(String nomPrenom) {
        this.nomPrenom = nomPrenom;
    }
    public String getLieuArrive() {
        return lieuArrive;
    }

    public void setLieuArrive(String lieuArrive) {
        this.lieuArrive = lieuArrive;
    }
    public String getLieuDepart() {
        return lieuDepart;
    }

    public void setLieuDepart(String lieuDepart) {
        this.lieuDepart = lieuDepart;
    }
    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    public String getDivers() {
        return divers;
    }

    public void setDivers(String divers) {
        this.divers = divers;
    }
    public String getPhoneImei() {
        return phoneImei;
    }

    public void setPhoneImei(String phoneImei) {
        this.phoneImei = phoneImei;
    }
    public String getPhoneSerialNumber() {
        return phoneSerialNumber;
    }

    public void setPhoneSerialNumber(String phoneSerialNumber) {
        this.phoneSerialNumber = phoneSerialNumber;
    }

    public PassagerEnum getPassagerType() {
        return passagerType;
    }

    public void setPassagerType(PassagerEnum passagerType) {
        this.passagerType = passagerType;
    }

    public String getTaxiDriverCode() {
        return taxiDriverCode;
    }

    public void setTaxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PassagerDTO)) return false;
        PassagerDTO that = (PassagerDTO) o;
        return Objects.equals(getPhone(), that.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPhone());
    }

    @Override
    public String toString() {
        return "PassagerDTO{" +
            ", phone='" + phone + "'" +
            ", nomPrenom='" + nomPrenom + "'" +
            ", lieuArrive='" + lieuArrive + "'" +
            ", lieuDepart='" + lieuDepart + "'" +
            ", latitude='" + latitude + "'" +
            ", longitude='" + longitude + "'" +
            ", divers='" + divers + "'" +
            ", phoneImei='" + phoneImei + "'" +
            ", phoneSerialNumber='" + phoneSerialNumber + "'" +
            '}';
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public PassagerDTO phone(String phone) {
        this.phone = phone;
        return this;
    }

    public PassagerDTO code(String code) {
        this.code = code;
        return this;
    }

    public PassagerDTO nomPrenom(String nomPrenom) {
        this.nomPrenom = nomPrenom;
        return this;
    }

    public PassagerDTO lieuArrive(String lieuArrive) {
        this.lieuArrive = lieuArrive;
        return this;
    }

    public PassagerDTO lieuDepart(String lieuDepart) {
        this.lieuDepart = lieuDepart;
        return this;
    }

    public PassagerDTO latitude(BigDecimal latitude) {
        this.latitude = latitude;
        return this;
    }

    public PassagerDTO longitude(BigDecimal longitude) {
        this.longitude = longitude;
        return this;
    }

    public PassagerDTO divers(String divers) {
        this.divers = divers;
        return this;
    }

    public PassagerDTO phoneImei(String phoneImei) {
        this.phoneImei = phoneImei;
        return this;
    }

    public PassagerDTO phoneSerialNumber(String phoneSerialNumber) {
        this.phoneSerialNumber = phoneSerialNumber;
        return this;
    }


    public PassagerDTO taxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
        return this;
    }

    public PassagerDTO passagerType(PassagerEnum passagerType) {
        this.passagerType = passagerType;
        return this;
    }

}
