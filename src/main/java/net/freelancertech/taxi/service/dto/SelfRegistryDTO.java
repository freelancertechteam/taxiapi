package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.api.typage.SituationEnum;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Organisation.
 */

public class SelfRegistryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String activatedCode;

    private String telephone;

    private LocalDate activatedCodeExpiredDate;


    private String organisationCode;

    private SituationEnum situation;
    private String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getActivatedCode() {
        return activatedCode;
    }

    public void setActivatedCode(String activatedCode) {
        this.activatedCode = activatedCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public LocalDate getActivatedCodeExpiredDate() {
        return activatedCodeExpiredDate;
    }

    public void setActivatedCodeExpiredDate(LocalDate activatedCodeExpiredDate) {
        this.activatedCodeExpiredDate = activatedCodeExpiredDate;
    }

    public SelfRegistryDTO activatedCode(String activatedCode) {
        this.activatedCode = activatedCode;
        return this;
    }

    public SelfRegistryDTO telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public SelfRegistryDTO activatedCodeExpiredDate(LocalDate activatedCodeExpiredDate) {
        this.activatedCodeExpiredDate = activatedCodeExpiredDate;
        return this;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public void setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
    }

    public SituationEnum getSituation() {
        return situation;
    }

    public void setSituation(SituationEnum situation) {
        this.situation = situation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SelfRegistryDTO)) return false;
        SelfRegistryDTO that = (SelfRegistryDTO) o;
        return Objects.equals(getTelephone(), that.getTelephone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTelephone());
    }


    public SelfRegistryDTO organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public SelfRegistryDTO situation(SituationEnum situation) {
        this.situation = situation;
        return this;
    }

    public SelfRegistryDTO emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }
}
