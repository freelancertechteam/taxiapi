package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.api.typage.SituationEnum;
import net.freelancertech.taxi.api.typage.SituationMatrimonialeEnum;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;


/**
 * A DTO for the TaxiDriver entity.
 */
public class TaxiDriverEditDTO implements ITaxiDriverDTO{


    @NotNull
    private LocalDate birthDate;

    @NotNull
    private String capacityNumber;

    @NotNull
    private String code;

    @NotNull
    private String drivingLicenseNumber;

    private String emailAddress;

    @Size(max = 60)
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String nicn;

    @NotNull
    private String placeOfBirth;

    @NotNull
    private String telephoneNumber;


    @NotNull
    private Integer version;

    private String nationalite;

    private String departementOrigine;

    private SituationMatrimonialeEnum situationMatrimoniale;

    private String domicile;

    private String telUrgent;

    private String numRecepisse;

    @NotNull
    private String cityCode;
    @NotNull
    private String organisationCode;

    private SituationEnum situation;


    private LocalDate capacityExpiredDate;

    private LocalDate drivingLicenseExpiredDate;

    private LocalDate nicnExpiredDate;


    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
    public String getCapacityNumber() {
        return capacityNumber;
    }

    public void setCapacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lasteName) {
        this.lastName = lasteName;
    }
    public String getNicn() {
        return nicn;
    }

    public void setNicn(String nicn) {
        this.nicn = nicn;
    }
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }
    public String getDepartementOrigine() {
        return departementOrigine;
    }

    public void setDepartementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
    }
    public SituationMatrimonialeEnum getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(SituationMatrimonialeEnum situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }
    public String getDomicile() {
        return domicile;
    }

    public void setDomicile(String domicile) {
        this.domicile = domicile;
    }
    public String getTelUrgent() {
        return telUrgent;
    }

    public void setTelUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
    }
    public String getNumRecepisse() {
        return numRecepisse;
    }

    public void setNumRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public void setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
    }

    public SituationEnum getSituation() {
        return situation;
    }

    public void setSituation(SituationEnum situation) {
        this.situation = situation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TaxiDriverEditDTO taxiDriverDTO = (TaxiDriverEditDTO) o;

        if ( ! Objects.equals(code, taxiDriverDTO.code)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(code);
    }

    @Override
    public String toString() {
        return "TaxiDriverDTO{" +
            ", birthDate='" + birthDate + "'" +
            ", capacityNumber='" + capacityNumber + "'" +
            ", code='" + code + "'" +
            ", drivingLicenseNumber='" + drivingLicenseNumber + "'" +
            ", emailAddress='" + emailAddress + "'" +
            ", firstName='" + firstName + "'" +
            ", lastName='" + lastName + "'" +
            ", nicn='" + nicn + "'" +
            ", placeOfBirth='" + placeOfBirth + "'" +
            ", telephoneNumber='" + telephoneNumber + "'" +
            ", version='" + version + "'" +
            ", nationalite='" + nationalite + "'" +
            ", departementOrigine='" + departementOrigine + "'" +
            ", situationMatrimoniale='" + situationMatrimoniale + "'" +
            ", domicile='" + domicile + "'" +
            ", telUrgent='" + telUrgent + "'" +
            ", numRecepisse='" + numRecepisse + "'" +
            '}';
    }

public String getNomPrenom() {
    return lastName+" "+firstName;
    }

    public TaxiDriverEditDTO birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public TaxiDriverEditDTO capacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
        return this;
    }

    public TaxiDriverEditDTO code(String code) {
        this.code = code;
        return this;
    }

    public TaxiDriverEditDTO drivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
        return this;
    }

    public TaxiDriverEditDTO emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public TaxiDriverEditDTO firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TaxiDriverEditDTO lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TaxiDriverEditDTO nicn(String nicn) {
        this.nicn = nicn;
        return this;
    }

    public TaxiDriverEditDTO placeOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public TaxiDriverEditDTO telephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public TaxiDriverEditDTO version(Integer version) {
        this.version = version;
        return this;
    }

    public TaxiDriverEditDTO nationalite(String nationalite) {
        this.nationalite = nationalite;
        return this;
    }

    public TaxiDriverEditDTO departementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
        return this;
    }

    public TaxiDriverEditDTO situationMatrimoniale(SituationMatrimonialeEnum situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
        return this;
    }

    public TaxiDriverEditDTO domicile(String domicile) {
        this.domicile = domicile;
        return this;
    }

    public TaxiDriverEditDTO telUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
        return this;
    }

    public TaxiDriverEditDTO numRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
        return this;
    }



    public TaxiDriverEditDTO situation(SituationEnum situation) {
        this.situation = situation;
        return this;
    }

    public TaxiDriverEditDTO cityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public TaxiDriverEditDTO organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public LocalDate getCapacityExpiredDate() {
        return capacityExpiredDate;
    }

    public void setCapacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
    }

    public LocalDate getDrivingLicenseExpiredDate() {
        return drivingLicenseExpiredDate;
    }

    public void setDrivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
    }

    public LocalDate getNicnExpiredDate() {
        return nicnExpiredDate;
    }

    public void setNicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
    }

    public TaxiDriverEditDTO capacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
        return this;
    }

    public TaxiDriverEditDTO drivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
        return this;
    }

    public TaxiDriverEditDTO nicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
        return this;
    }
}
