package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Vehicule entity.
 */
public class VehiculeDTO implements Serializable {


    @NotNull
    @Size(max = 35)
    private String immatriculation;

    @NotNull
    private String proprietaire;

    @NotNull
    private String numPortiere;

    @NotNull
    @Size(max = 15)
    private String cniProprietaire;

    private String numCarteGrise;


    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }
    public String getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }
    public String getNumPortiere() {
        return numPortiere;
    }

    public void setNumPortiere(String numPortiere) {
        this.numPortiere = numPortiere;
    }
    public String getCniProprietaire() {
        return cniProprietaire;
    }

    public void setCniProprietaire(String cniProprietaire) {
        this.cniProprietaire = cniProprietaire;
    }
    public String getNumCarteGrise() {
        return numCarteGrise;
    }

    public void setNumCarteGrise(String numCarteGrise) {
        this.numCarteGrise = numCarteGrise;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VehiculeDTO vehiculeDTO = (VehiculeDTO) o;

        if ( ! Objects.equals(numCarteGrise, vehiculeDTO.numCarteGrise)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(numCarteGrise);
    }

    @Override
    public String toString() {
        return "VehiculeDTO{" +
            ", immatriculation='" + immatriculation + "'" +
            ", proprietaire='" + proprietaire + "'" +
            ", numPortiere='" + numPortiere + "'" +
            ", cniProprietaire='" + cniProprietaire + "'" +
            ", numCarteGrise='" + numCarteGrise + "'" + 
            '}';
    }
}
