package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A TaxiDriver.
 */

public class RecepisseTaxiDriverDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    @NotNull
    private String numRecepisse;
    @NotNull
    private LocalDate delivranceDate;
    @NotNull
    private LocalDate validityDate;

    @NotNull
    private Integer periodeValidity;

    @NotNull
    private String telephoneNumber;
    private String emailAddress;

    @NotNull
    private String capacityNumber;
    @NotNull
    private String drivingLicenseNumber;
    @NotNull
    private String nicn;
    @Size(max = 60)
    private String firstName;

    @NotNull
    private String lastName;
    private String cityCode;

    private String organisationCode;


    public String getNumRecepisse() {
        return numRecepisse;
    }

    public void setNumRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
    }

    public LocalDate getDelivranceDate() {
        return delivranceDate;
    }

    public void setDelivranceDate(LocalDate delivranceDate) {
        this.delivranceDate = delivranceDate;
    }

    public LocalDate getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(LocalDate validityDate) {
        this.validityDate = validityDate;
    }

    public Integer getPeriodeValidity() {
        return periodeValidity;
    }

    public void setPeriodeValidity(Integer periodeValidity) {
        this.periodeValidity = periodeValidity;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCapacityNumber() {
        return capacityNumber;
    }

    public void setCapacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public String getNicn() {
        return nicn;
    }

    public void setNicn(String nicn) {
        this.nicn = nicn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public void setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
    }

    public RecepisseTaxiDriverDTO numRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
        return this;
    }

    public RecepisseTaxiDriverDTO delivranceDate(LocalDate delivranceDate) {
        this.delivranceDate = delivranceDate;
        return this;
    }

    public RecepisseTaxiDriverDTO validityDate(LocalDate validityDate) {
        this.validityDate = validityDate;
        return this;
    }

    public RecepisseTaxiDriverDTO periodeValidity(Integer periodeValidity) {
        this.periodeValidity = periodeValidity;
        return this;
    }

    public RecepisseTaxiDriverDTO telephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public RecepisseTaxiDriverDTO emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public RecepisseTaxiDriverDTO capacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
        return this;
    }

    public RecepisseTaxiDriverDTO drivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
        return this;
    }

    public RecepisseTaxiDriverDTO nicn(String nicn) {
        this.nicn = nicn;
        return this;
    }

    public RecepisseTaxiDriverDTO firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public RecepisseTaxiDriverDTO lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public RecepisseTaxiDriverDTO cityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public RecepisseTaxiDriverDTO organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecepisseTaxiDriverDTO)) return false;
        RecepisseTaxiDriverDTO that = (RecepisseTaxiDriverDTO) o;
        return Objects.equals(getNumRecepisse(), that.getNumRecepisse());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumRecepisse());
    }
}
