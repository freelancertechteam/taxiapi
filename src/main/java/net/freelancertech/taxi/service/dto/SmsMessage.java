package net.freelancertech.taxi.service.dto;

/**
 * Created by simon on 07/03/2017.
 */
public class SmsMessage {
    private String phoneNumber;
    private String message;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SmsMessage phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }


    public SmsMessage message(String message) {
        this.message = message;
        return this;
    }
}
