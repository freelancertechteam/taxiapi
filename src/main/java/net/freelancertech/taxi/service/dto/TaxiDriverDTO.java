package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.api.typage.SituationEnum;
import net.freelancertech.taxi.api.typage.SituationMatrimonialeEnum;
import net.freelancertech.taxi.api.typage.TimbreFiscalEnum;
import org.springframework.format.annotation.DateTimeFormat;

import javax.print.attribute.standard.MediaSize;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;


/**
 * A DTO for the TaxiDriver entity.
 */
public class TaxiDriverDTO implements ITaxiDriverDTO {


    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate birthDate;

    @NotNull
    private String capacityNumber;


    private String code;

    @NotNull
    private String drivingLicenseNumber;

    private String emailAddress;

    @Size(max = 60)
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String nicn;

    @NotNull
    private String placeOfBirth;

    @NotNull
    private String telephoneNumber;



    private Integer version;

    private String nationalite;

    private String departementOrigine;

    private SituationMatrimonialeEnum situationMatrimoniale;

    private String domicile;

    private String telUrgent;




    private String cityCode;

    private String organisationCode;

    private SituationEnum situation;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate capacityExpiredDate;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate drivingLicenseExpiredDate;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate nicnExpiredDate;

    private boolean hasPhoto;
    private boolean hasCniPiece;
    private boolean hasCapacityPiece;
    private boolean hasDrivingLicensePiece;

    /**
     * periode de validite du badge.
     */
    private LocalDate validityBadgeDate;
    private String telephoneNumberBis;
    /**
     * lieu d etablissement de la cni.
     */
    private String lieuNicn;
    private TimbreFiscalEnum timbreFiscal;
    private String numRecepisse;


    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
    public String getCapacityNumber() {
        return capacityNumber;
    }

    public void setCapacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lasteName) {
        this.lastName = lasteName;
    }
    public String getNicn() {
        return nicn;
    }

    public void setNicn(String nicn) {
        this.nicn = nicn;
    }
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }
    public String getDepartementOrigine() {
        return departementOrigine;
    }

    public void setDepartementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
    }
    public SituationMatrimonialeEnum getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(SituationMatrimonialeEnum situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }
    public String getDomicile() {
        return domicile;
    }

    public void setDomicile(String domicile) {
        this.domicile = domicile;
    }
    public String getTelUrgent() {
        return telUrgent;
    }

    public void setTelUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
    }
    public String getNumRecepisse() {
        return numRecepisse;
    }

    public void setNumRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public void setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
    }

    public SituationEnum getSituation() {
        return situation;
    }

    public void setSituation(SituationEnum situation) {
        this.situation = situation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TaxiDriverDTO taxiDriverDTO = (TaxiDriverDTO) o;

        if ( ! Objects.equals(code, taxiDriverDTO.code)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(code);
    }

    @Override
    public String toString() {
        return "TaxiDriverDTO{" +
            ", birthDate='" + birthDate + "'" +
            ", capacityNumber='" + capacityNumber + "'" +
            ", code='" + code + "'" +
            ", drivingLicenseNumber='" + drivingLicenseNumber + "'" +
            ", emailAddress='" + emailAddress + "'" +
            ", firstName='" + firstName + "'" +
            ", lastName='" + lastName + "'" +
            ", nicn='" + nicn + "'" +
            ", placeOfBirth='" + placeOfBirth + "'" +
            ", telephoneNumber='" + telephoneNumber + "'" +
            ", version='" + version + "'" +
            ", nationalite='" + nationalite + "'" +
            ", departementOrigine='" + departementOrigine + "'" +
            ", situationMatrimoniale='" + situationMatrimoniale + "'" +
            ", domicile='" + domicile + "'" +
            ", telUrgent='" + telUrgent + "'" +
            ", numRecepisse='" + numRecepisse + "'" +
            '}';
    }

public String getNomPrenom() {
    return lastName+" "+firstName;
    }

    public TaxiDriverDTO birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public TaxiDriverDTO capacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
        return this;
    }

    public TaxiDriverDTO code(String code) {
        this.code = code;
        return this;
    }

    public TaxiDriverDTO drivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
        return this;
    }

    public TaxiDriverDTO emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public TaxiDriverDTO firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TaxiDriverDTO lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TaxiDriverDTO nicn(String nicn) {
        this.nicn = nicn;
        return this;
    }

    public TaxiDriverDTO placeOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public TaxiDriverDTO telephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public TaxiDriverDTO version(Integer version) {
        this.version = version;
        return this;
    }

    public TaxiDriverDTO nationalite(String nationalite) {
        this.nationalite = nationalite;
        return this;
    }

    public TaxiDriverDTO departementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
        return this;
    }

    public TaxiDriverDTO situationMatrimoniale(SituationMatrimonialeEnum situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
        return this;
    }

    public TaxiDriverDTO domicile(String domicile) {
        this.domicile = domicile;
        return this;
    }

    public TaxiDriverDTO telUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
        return this;
    }

    public TaxiDriverDTO numRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
        return this;
    }



    public TaxiDriverDTO situation(SituationEnum situation) {
        this.situation = situation;
        return this;
    }

    public TaxiDriverDTO cityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public TaxiDriverDTO organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public LocalDate getCapacityExpiredDate() {
        return capacityExpiredDate;
    }

    public void setCapacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
    }

    public LocalDate getDrivingLicenseExpiredDate() {
        return drivingLicenseExpiredDate;
    }

    public void setDrivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
    }

    public LocalDate getNicnExpiredDate() {
        return nicnExpiredDate;
    }

    public void setNicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
    }

    public TaxiDriverDTO capacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
        return this;
    }

    public TaxiDriverDTO drivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
        return this;
    }

    public TaxiDriverDTO nicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
        return this;
    }

    public boolean isHasPhoto() {
        return hasPhoto;
    }

    public void setHasPhoto(boolean hasPhoto) {
        this.hasPhoto = hasPhoto;
    }

    public boolean isHasCniPiece() {
        return hasCniPiece;
    }

    public void setHasCniPiece(boolean hasCniPiece) {
        this.hasCniPiece = hasCniPiece;
    }

    public boolean isHasCapacityPiece() {
        return hasCapacityPiece;
    }

    public void setHasCapacityPiece(boolean hasCapacityPiece) {
        this.hasCapacityPiece = hasCapacityPiece;
    }

    public boolean isHasDrivingLicensePiece() {
        return hasDrivingLicensePiece;
    }

    public void setHasDrivingLicensePiece(boolean hasDrivingLicensePiece) {
        this.hasDrivingLicensePiece = hasDrivingLicensePiece;
    }

    public TaxiDriverDTO hasPhoto(boolean hasPhoto) {
        this.hasPhoto = hasPhoto;
        return this;
    }

    public TaxiDriverDTO hasCniPiece(boolean hasCniPiece) {
        this.hasCniPiece = hasCniPiece;
        return this;
    }

    public TaxiDriverDTO hasCapacityPiece(boolean hasCapacityPiece) {
        this.hasCapacityPiece = hasCapacityPiece;
        return this;
    }

    public TaxiDriverDTO hasDrivingLicensePiece(boolean hasDrivingLicensePiece) {
        this.hasDrivingLicensePiece = hasDrivingLicensePiece;
        return this;
    }

    public LocalDate getValidityBadgeDate() {
        return validityBadgeDate;
    }

    public void setValidityBadgeDate(LocalDate validityBadgeDate) {
        this.validityBadgeDate = validityBadgeDate;
    }

    public String getTelephoneNumberBis() {
        return telephoneNumberBis;
    }

    public void setTelephoneNumberBis(String telephoneNumberBis) {
        this.telephoneNumberBis = telephoneNumberBis;
    }

    public String getLieuNicn() {
        return lieuNicn;
    }

    public void setLieuNicn(String lieuNicn) {
        this.lieuNicn = lieuNicn;
    }

    public TimbreFiscalEnum getTimbreFiscal() {
        return timbreFiscal;
    }

    public void setTimbreFiscal(TimbreFiscalEnum timbreFiscal) {
        this.timbreFiscal = timbreFiscal;
    }

    public TaxiDriverDTO validityBadgeDate(LocalDate validityBadgeDate) {
        this.validityBadgeDate = validityBadgeDate;
        return this;
    }

    public TaxiDriverDTO telephoneNumberBis(String telephoneNumberBis) {
        this.telephoneNumberBis = telephoneNumberBis;
        return this;
    }

    public TaxiDriverDTO lieuNicn(String lieuNicn) {
        this.lieuNicn = lieuNicn;
        return this;
    }

    public TaxiDriverDTO timbreFiscal(TimbreFiscalEnum timbreFiscal) {
        this.timbreFiscal = timbreFiscal;
        return this;
    }
}
