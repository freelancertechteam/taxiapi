package net.freelancertech.taxi.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the City entity.
 */
public class CityDTO implements Serializable {

    @NotNull
    @Size(max = 10)
    private String code;

    @NotNull
    @Size(max = 60)
    private String name;

    private String description;
    


    private Integer version;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public CityDTO code(String code) {
        this.code = code;
        return this;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public CityDTO name(String name) {
        this.name = name;
        return this;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CityDTO description(String description) {
        this.description = description;
        return this;
    }

    
    

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public CityDTO version(Integer version) {
        this.version = version;
        return this;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CityDTO)) return false;
        CityDTO cityDTO = (CityDTO) o;
        return Objects.equals(getCode(), cityDTO.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }

    @Override
    public String toString() {
        return "CityDTO{" +
            ", code='" + code + "'" +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", version='" + version + "'" +
            '}';
    }
}
