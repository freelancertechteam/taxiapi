package net.freelancertech.taxi.service.dto;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Score entity.
 * <p>
 * Created by jnguetsop on 09/01/2017.
 */
public class ScoreParam implements Serializable {

    @NotNull
    @Size(min = 1, max = 60)
    private String name;

    @Email
    @Size(max = 30)
    private String email;

    @NotNull
    @Size(min = 1, max = 30)
    private String telephone;

    @NotNull
    @DecimalMin(value = "1")
    @DecimalMax(value = "5")
    private Byte score;



    public String getName() {
        return name;
    }

    public ScoreParam name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public ScoreParam email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public ScoreParam telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Byte getScore() {
        return score;
    }

    public ScoreParam score(Byte score) {
        this.score = score;
        return this;
    }

    public void setScore(Byte score) {
        this.score = score;
    }




}
