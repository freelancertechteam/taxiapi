package net.freelancertech.taxi.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Photo entity.
 */
public class PhotoDTO implements Serializable {



    private String description;

    @NotNull
    private byte[] valeur;

    private String valeurContentType;


    private String taxiDriverCode;

    private Integer version;

    private Long taxiDriverId;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public PhotoDTO description(String description) {
        this.description = description;
        return this;
    }
    public byte[] getValeur() {
        return valeur;
    }

    public void setValeur(byte[] valeur) {
        this.valeur = valeur;
    }
    public PhotoDTO valeur(byte[] valeur) {
        this.valeur = valeur;
        return this;
    }

    public String getValeurContentType() {
        return valeurContentType;
    }

    public void setValeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
    }

    public PhotoDTO valeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
        return this;
    }

    public String getTaxiDriverCode() {
        return taxiDriverCode;
    }

    public void setTaxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
    }


    public Long getTaxiDriverId() {
        return taxiDriverId;
    }

    public void setTaxiDriverId(Long taxiDriverId) {
        this.taxiDriverId = taxiDriverId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhotoDTO)) return false;
        PhotoDTO photoDTO = (PhotoDTO) o;
        return Objects.equals(getTaxiDriverCode(), photoDTO.getTaxiDriverCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTaxiDriverCode());
    }

    @Override
    public String toString() {
        return "PhotoDTO{" +
            ", description='" + description + "'" +
            ", valeur='" + valeur + "'" +
            ", taxiDriverId='" + taxiDriverId + "'" +
            ", taxiDriverCode='" + taxiDriverCode + "'" +
            '}';
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
