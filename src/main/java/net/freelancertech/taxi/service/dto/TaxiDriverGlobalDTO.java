package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.api.typage.SituationEnum;
import net.freelancertech.taxi.api.typage.TimbreFiscalEnum;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;


/**
 * A DTO for the TaxiDriver entity.
 */
public class TaxiDriverGlobalDTO implements Serializable {

    @NotNull
    private String capacityNumber;

    @NotNull
    private String code;

    @NotNull
    private String drivingLicenseNumber;

    private String emailAddress;

    @Size(max = 60)
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String nicn;

    @NotNull
    private String telephoneNumber;


    private String nationalite;

    private String departementOrigine;

    private String situationMatrimoniale;

    private String domicile;

    private String telUrgent;

    private String cityCode;

    private String cityDesignation;

    private String organisationCode;

    private String organisationDesignation;

    private SituationEnum situation;


    private LocalDate capacityExpiredDate;

    private LocalDate drivingLicenseExpiredDate;

    private LocalDate nicnExpiredDate;

    private boolean hasPhoto;

    /**
     * periode de validite du badge.
     */
    private LocalDate validityBadgeDate;
    private String telephoneNumberBis;
    /**
     * lieu d etablissement de la cni.
     */
    private String lieuNicn;
    private TimbreFiscalEnum timbreFiscal;
    private String numRecepisse;

    public String getCapacityNumber() {
        return capacityNumber;
    }

    public void setCapacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNicn() {
        return nicn;
    }

    public void setNicn(String nicn) {
        this.nicn = nicn;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public String getDepartementOrigine() {
        return departementOrigine;
    }

    public void setDepartementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
    }

    public String getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(String situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public String getDomicile() {
        return domicile;
    }

    public void setDomicile(String domicile) {
        this.domicile = domicile;
    }

    public String getTelUrgent() {
        return telUrgent;
    }

    public void setTelUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityDesignation() {
        return cityDesignation;
    }

    public void setCityDesignation(String cityDesignation) {
        this.cityDesignation = cityDesignation;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public void setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
    }

    public String getOrganisationDesignation() {
        return organisationDesignation;
    }

    public void setOrganisationDesignation(String organisationDesignation) {
        this.organisationDesignation = organisationDesignation;
    }

    public SituationEnum getSituation() {
        return situation;
    }

    public void setSituation(SituationEnum situation) {
        this.situation = situation;
    }

    public TaxiDriverGlobalDTO capacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
        return this;
    }

    public TaxiDriverGlobalDTO code(String code) {
        this.code = code;
        return this;
    }

    public TaxiDriverGlobalDTO drivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
        return this;
    }

    public TaxiDriverGlobalDTO emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public TaxiDriverGlobalDTO firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TaxiDriverGlobalDTO lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TaxiDriverGlobalDTO nicn(String nicn) {
        this.nicn = nicn;
        return this;
    }

    public TaxiDriverGlobalDTO telephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public TaxiDriverGlobalDTO nationalite(String nationalite) {
        this.nationalite = nationalite;
        return this;
    }

    public TaxiDriverGlobalDTO departementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
        return this;
    }

    public TaxiDriverGlobalDTO situationMatrimoniale(String situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
        return this;
    }

    public TaxiDriverGlobalDTO domicile(String domicile) {
        this.domicile = domicile;
        return this;
    }

    public TaxiDriverGlobalDTO telUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
        return this;
    }

    public TaxiDriverGlobalDTO cityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public TaxiDriverGlobalDTO cityDesignation(String cityDesignation) {
        this.cityDesignation = cityDesignation;
        return this;
    }

    public TaxiDriverGlobalDTO organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public TaxiDriverGlobalDTO organisationDesignation(String organisationDesignation) {
        this.organisationDesignation = organisationDesignation;
        return this;
    }

    public TaxiDriverGlobalDTO situation(SituationEnum situation) {
        this.situation = situation;
        return this;
    }

    public LocalDate getCapacityExpiredDate() {
        return capacityExpiredDate;
    }

    public void setCapacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
    }

    public LocalDate getDrivingLicenseExpiredDate() {
        return drivingLicenseExpiredDate;
    }

    public void setDrivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
    }

    public LocalDate getNicnExpiredDate() {
        return nicnExpiredDate;
    }

    public void setNicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
    }

    public TaxiDriverGlobalDTO capacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
        return this;
    }

    public TaxiDriverGlobalDTO drivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
        return this;
    }

    public TaxiDriverGlobalDTO nicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
        return this;
    }

    public boolean isHasPhoto() {
        return hasPhoto;
    }

    public void setHasPhoto(boolean hasPhoto) {
        this.hasPhoto = hasPhoto;
    }

    public TaxiDriverGlobalDTO hasPhoto(boolean hasPhoto) {
        this.hasPhoto = hasPhoto;
        return this;
    }

    public LocalDate getValidityBadgeDate() {
        return validityBadgeDate;
    }

    public void setValidityBadgeDate(LocalDate validityBadgeDate) {
        this.validityBadgeDate = validityBadgeDate;
    }

    public String getTelephoneNumberBis() {
        return telephoneNumberBis;
    }

    public void setTelephoneNumberBis(String telephoneNumberBis) {
        this.telephoneNumberBis = telephoneNumberBis;
    }

    public String getLieuNicn() {
        return lieuNicn;
    }

    public void setLieuNicn(String lieuNicn) {
        this.lieuNicn = lieuNicn;
    }

    public TimbreFiscalEnum getTimbreFiscal() {
        return timbreFiscal;
    }

    public void setTimbreFiscal(TimbreFiscalEnum timbreFiscal) {
        this.timbreFiscal = timbreFiscal;
    }

    public String getNumRecepisse() {
        return numRecepisse;
    }

    public void setNumRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
    }

    public TaxiDriverGlobalDTO validityBadgeDate(LocalDate validityBadgeDate) {
        this.validityBadgeDate = validityBadgeDate;
        return this;
    }

    public TaxiDriverGlobalDTO telephoneNumberBis(String telephoneNumberBis) {
        this.telephoneNumberBis = telephoneNumberBis;
        return this;
    }

    public TaxiDriverGlobalDTO lieuNicn(String lieuNicn) {
        this.lieuNicn = lieuNicn;
        return this;
    }

    public TaxiDriverGlobalDTO timbreFiscal(TimbreFiscalEnum timbreFiscal) {
        this.timbreFiscal = timbreFiscal;
        return this;
    }

    public TaxiDriverGlobalDTO numRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
        return this;
    }
}
