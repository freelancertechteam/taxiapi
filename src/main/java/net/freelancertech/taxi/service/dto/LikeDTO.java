package net.freelancertech.taxi.service.dto;

import net.freelancertech.taxi.service.dto.enumeration.LikeEnum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the Like entity.
 */
public class LikeDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 1, max = 60)
    private String name;

    @Size(max = 30)
    private String email;

    @NotNull
    @Size(min = 1, max = 30)
    private String telephone;

    @NotNull
    private LikeEnum valeur;

    @NotNull
    private ZonedDateTime createdDate;


    private String taxiDriverCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public LikeEnum getValeur() {
        return valeur;
    }

    public void setValeur(LikeEnum valeur) {
        this.valeur = valeur;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }


    public String getTaxiDriverCode() {
        return taxiDriverCode;
    }

    public void setTaxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LikeDTO likeDTO = (LikeDTO) o;

        if ( ! Objects.equals(id, likeDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "LikeDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", email='" + email + "'" +
            ", telephone='" + telephone + "'" +
            ", valeur='" + valeur + "'"  +
            '}';
    }
}
