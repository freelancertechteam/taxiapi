package net.freelancertech.taxi.api.typage;

import java.util.Objects;

/**
 * Created by simon on 09/02/2017.
 */
public enum AuthorityAllRoleEnum {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_ANONYMOUS,
    ROLE_PRE_AUTHENTICATED,
    ROLE_ATTENTE_ACTIVATION,
    ROLE_SYNDICAT_ADMIN,
    ROLE_SYNDICAT,
    ROLE_AGENT_PUBLIC_ADMIN,
    ROLE_AGENT_PUBLIC,
    ROLE_SOCLE_ADMIN,
    ROLE_SOCLE,
    ROLE_MEMBRE;

}
