package net.freelancertech.taxi.api.typage;

/**
 * Created by simon on 25/01/2017.
 */
public enum AuthoritiyAdminRoleEnum {
    ROLE_SYNDICAT_ADMIN,
    ROLE_AGENT_PUBLIC_ADMIN,
    ROLE_SOCLE_ADMIN
}
