package net.freelancertech.taxi.api.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by simon on 06/01/2017.
 */
public class FluxHandleDTO {
    @NotNull
    private byte[] valeur;

    private String valeurContentType;


    public byte[] getValeur() {
        return valeur;
    }

    public void setValeur(byte[] valeur) {
        this.valeur = valeur;
    }

    public FluxHandleDTO valeur(byte[] valeur) {
        this.valeur = valeur;
        return this;
    }

    public String getValeurContentType() {
        return valeurContentType;
    }

    public void setValeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
    }

    public FluxHandleDTO valeurContentType(String valeurContentType) {
        this.valeurContentType = valeurContentType;
        return this;
    }
}
