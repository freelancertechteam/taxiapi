package net.freelancertech.taxi.api.dto;

import net.freelancertech.taxi.dto.AbstractStatsDto;
import net.freelancertech.taxi.dto.syndicat.SyndicatStatsDto;
import net.freelancertech.taxi.service.dto.UserDTO;

import java.util.Map;

/**
 * Created by simon on 18/01/2017.
 */
public class ConnectedUserInformation {
    public static final String ATTENTE_ACTIVATION = "attenteActivation";
    private UserDTO user;
    private byte[] photos;
    private String photoContentType;
    private Map<String,String> notifications;

    private SyndicatStatsDto syndicatStatsDto;

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public byte[] getPhotos() {
        return photos;
    }

    public void setPhotos(byte[] photos) {
        this.photos = photos;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public Map<String, String> getNotifications() {
        return notifications;
    }

    public void setNotifications(Map<String, String> notifications) {
        this.notifications = notifications;
    }

    public SyndicatStatsDto getSyndicatStatsDto() {
        return syndicatStatsDto;
    }

    public void setSyndicatStatsDto(SyndicatStatsDto syndicatStatsDto) {
        this.syndicatStatsDto = syndicatStatsDto;
    }

    public ConnectedUserInformation user(UserDTO user) {
        this.user = user;
        return this;
    }

    public ConnectedUserInformation photos(byte[] photos) {
        this.photos = photos;
        return this;
    }

    public ConnectedUserInformation notifications(Map<String, String> notifications) {
        this.notifications = notifications;
        return this;
    }

    public ConnectedUserInformation photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }


    public ConnectedUserInformation syndicatStatsDto(SyndicatStatsDto syndicatStatsDto) {
        this.syndicatStatsDto = syndicatStatsDto;
        return this;
    }
}
