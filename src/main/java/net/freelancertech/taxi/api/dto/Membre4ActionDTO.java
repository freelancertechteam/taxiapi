package net.freelancertech.taxi.api.dto;

import net.freelancertech.taxi.api.typage.SituationEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by simon on 22/01/2017.
 */
public class Membre4ActionDTO {
    @NotNull
    @NotEmpty
    private String taxiDriverCode;
    @NotNull
    private SituationEnum situationEnum;
    @NotNull
    private Integer version;


    public String getTaxiDriverCode() {
        return taxiDriverCode;
    }

    public void setTaxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
    }

    public SituationEnum getSituationEnum() {
        return situationEnum;
    }

    public void setSituationEnum(SituationEnum situationEnum) {
        this.situationEnum = situationEnum;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Membre4ActionDTO taxiDriverCode(String taxiDriverCode) {
        this.taxiDriverCode = taxiDriverCode;
        return this;
    }

    public Membre4ActionDTO situationEnum(SituationEnum situationEnum) {
        this.situationEnum = situationEnum;
        return this;
    }

    public Membre4ActionDTO version(Integer version) {
        this.version = version;
        return this;
    }
}
