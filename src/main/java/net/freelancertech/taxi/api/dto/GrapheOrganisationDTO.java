package net.freelancertech.taxi.api.dto;

import java.util.Objects;

/**
 * Created by simon on 22/01/2017.
 */
public class GrapheOrganisationDTO {
    private String code;
    private String designation;
    private String abreviation;
    private Long nbreDriver;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getNbreDriver() {
        return nbreDriver;
    }

    public void setNbreDriver(Long nbreDriver) {
        this.nbreDriver = nbreDriver;
    }

    public GrapheOrganisationDTO code(String code) {
        this.code = code;
        return this;
    }

    public GrapheOrganisationDTO designation(String designation) {
        this.designation = designation;
        return this;
    }

    public GrapheOrganisationDTO nbreDriver(Long nbreDriver) {
        this.nbreDriver = nbreDriver;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GrapheOrganisationDTO)) return false;
        GrapheOrganisationDTO that = (GrapheOrganisationDTO) o;
        return Objects.equals(getCode(), that.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    public GrapheOrganisationDTO abreviation(String abreviation) {
        this.abreviation = abreviation;
        return this;
    }
}
