package net.freelancertech.taxi.api;

/**
 * Created by simon on 02/01/2017.
 */
public enum OrganisationEnum {
    NO_NATURE("Non_DIFINE"),SYNDICAT("Syndicat"),SOCLE("Administrateur"),ETAT("Agent_Public");
    private final String nature;

    OrganisationEnum(String nature){
       this.nature=nature;
    }

    public String getNature() {
        return nature;
    }
}
