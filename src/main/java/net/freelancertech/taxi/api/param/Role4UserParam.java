package net.freelancertech.taxi.api.param;

import net.freelancertech.taxi.api.typage.AuthorityAllRoleEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by simon on 09/02/2017.
 */
public class Role4UserParam {
    @NotEmpty
    @NotNull
    private String username;
    @NotNull
    private List<AuthorityAllRoleEnum> roles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<AuthorityAllRoleEnum> getRoles() {
        return roles;
    }

    public void setRoles(List<AuthorityAllRoleEnum> roles) {
        this.roles = roles;
    }

    public Role4UserParam username(String username) {
        this.username = username;
        return this;
    }

    public Role4UserParam roles(List<AuthorityAllRoleEnum> roles) {
        this.roles = roles;
        return this;
    }
}
