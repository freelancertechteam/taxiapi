package net.freelancertech.taxi.api.param;

import net.freelancertech.taxi.api.OrganisationEnum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


/**
 * A DTO for the Organisation entity.
 */
public class OrganisationNewPARAM implements Serializable {


    @NotNull
    private String designation;


    private String description;

    @NotNull
    @Size(max = 155)
    private String abreviation;

    private byte[] logo;

    private String logoContentType;
    @NotNull
    private OrganisationEnum nature;

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public OrganisationNewPARAM designation(String designation) {
        this.designation = designation;
        return this;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OrganisationNewPARAM description(String description) {
        this.description = description;
        return this;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }
    public OrganisationNewPARAM abreviation(String abreviation) {
        this.abreviation = abreviation;
        return this;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }
    public OrganisationNewPARAM logo(byte[] logo) {
        this.logo = logo;
        return this;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }
    public OrganisationNewPARAM logoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
        return this;
    }

    public OrganisationEnum getNature() {
        return nature;
    }

    public void setNature(OrganisationEnum nature) {
        this.nature = nature;
    }

    public OrganisationNewPARAM nature(OrganisationEnum nature) {
        this.nature = nature;
        return this;
    }


    @Override
    public String toString() {
        return "OrganisationDTO{" +
            ", designation='" + designation + "'" +
            ", description='" + description + "'" +
            ", abreviation='" + abreviation + "'" +
            '}';
    }
}
