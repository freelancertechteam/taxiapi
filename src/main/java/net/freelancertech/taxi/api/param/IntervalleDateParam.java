package net.freelancertech.taxi.api.param;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.ZonedDateTime;

/**
 * Created by simon on 01/02/2017.
 */
public class IntervalleDateParam {

    @NotNull
    @Past
    private ZonedDateTime debut;
    @NotNull
    @Past
    private ZonedDateTime fin;

    public ZonedDateTime getDebut() {
        return debut;
    }

    public void setDebut(ZonedDateTime debut) {
        this.debut = debut;
    }

    public ZonedDateTime getFin() {
        return fin;
    }

    public void setFin(ZonedDateTime fin) {
        this.fin = fin;
    }

    public IntervalleDateParam debut(ZonedDateTime debut) {
        this.debut = debut;
        return this;
    }

    public IntervalleDateParam fin(ZonedDateTime fin) {
        this.fin = fin;
        return this;
    }
}
