package net.freelancertech.taxi.api.param;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A TaxiDriver.
 */

public class RecepisseTaxiDriverParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private LocalDate validityDate;

    @NotNull
    private Integer periodeValidity;

    @NotNull
    private String telephoneNumber;
    private String emailAddress;

    @NotNull
    private String capacityNumber;
    @NotNull
    private String drivingLicenseNumber;
    @NotNull
    private String nicn;
    @Size(max = 60)
    private String firstName;

    @NotNull
    private String lastName;


    public LocalDate getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(LocalDate validityDate) {
        this.validityDate = validityDate;
    }

    public Integer getPeriodeValidity() {
        return periodeValidity;
    }

    public void setPeriodeValidity(Integer periodeValidity) {
        this.periodeValidity = periodeValidity;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCapacityNumber() {
        return capacityNumber;
    }

    public void setCapacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public String getNicn() {
        return nicn;
    }

    public void setNicn(String nicn) {
        this.nicn = nicn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public RecepisseTaxiDriverParam validityDate(LocalDate validityDate) {
        this.validityDate = validityDate;
        return this;
    }

    public RecepisseTaxiDriverParam periodeValidity(Integer periodeValidity) {
        this.periodeValidity = periodeValidity;
        return this;
    }

    public RecepisseTaxiDriverParam telephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public RecepisseTaxiDriverParam emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public RecepisseTaxiDriverParam capacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
        return this;
    }

    public RecepisseTaxiDriverParam drivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
        return this;
    }

    public RecepisseTaxiDriverParam nicn(String nicn) {
        this.nicn = nicn;
        return this;
    }

    public RecepisseTaxiDriverParam firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public RecepisseTaxiDriverParam lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

}
