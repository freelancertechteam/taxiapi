package net.freelancertech.taxi.api.param;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


/**
 * A DTO for the City entity.
 */
public class CityNewPARAM implements Serializable {

    @NotNull
    @Size(max = 60)
    private String name;

    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CityNewPARAM name(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CityNewPARAM description(String description) {
        this.description = description;
        return this;
    }
}
