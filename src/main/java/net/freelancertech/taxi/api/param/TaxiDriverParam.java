package net.freelancertech.taxi.api.param;

import net.freelancertech.taxi.api.typage.SituationEnum;
import net.freelancertech.taxi.api.typage.SituationMatrimonialeEnum;
import net.freelancertech.taxi.api.typage.TimbreFiscalEnum;
import net.freelancertech.taxi.service.dto.ITaxiDriverDTO;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;


/**
 * A DTO for the TaxiDriver entity.
 */
public class TaxiDriverParam {


    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate birthDate;

    @NotNull
    private String capacityNumber;

    @NotNull
    private String drivingLicenseNumber;

    private String emailAddress;

    @Size(max = 60)
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String nicn;

    @NotNull
    private String placeOfBirth;

    @NotNull
    private String telephoneNumber;

    private String nationalite;

    private String departementOrigine;

    private SituationMatrimonialeEnum situationMatrimoniale;

    private String domicile;

    private String telUrgent;

    private String cityCode;

    private String organisationCode;

    private SituationEnum situation;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate capacityExpiredDate;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate drivingLicenseExpiredDate;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate nicnExpiredDate;
    /**
     * periode de validite du badge.
     */
    private LocalDate validityBadgeDate;
    private String telephoneNumberBis;
    /**
     * lieu d etablissement de la cni.
     */
    private String lieuNicn;
    private TimbreFiscalEnum timbreFiscal;
    private String numRecepisse;


    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
    public String getCapacityNumber() {
        return capacityNumber;
    }

    public void setCapacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lasteName) {
        this.lastName = lasteName;
    }
    public String getNicn() {
        return nicn;
    }

    public void setNicn(String nicn) {
        this.nicn = nicn;
    }
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }
    public String getDepartementOrigine() {
        return departementOrigine;
    }

    public void setDepartementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
    }
    public SituationMatrimonialeEnum getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(SituationMatrimonialeEnum situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }
    public String getDomicile() {
        return domicile;
    }

    public void setDomicile(String domicile) {
        this.domicile = domicile;
    }
    public String getTelUrgent() {
        return telUrgent;
    }

    public void setTelUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
    }
    public String getNumRecepisse() {
        return numRecepisse;
    }

    public void setNumRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public void setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
    }

    public SituationEnum getSituation() {
        return situation;
    }

    public void setSituation(SituationEnum situation) {
        this.situation = situation;
    }


public String getNomPrenom() {
    return lastName+" "+firstName;
    }

    public TaxiDriverParam birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public TaxiDriverParam capacityNumber(String capacityNumber) {
        this.capacityNumber = capacityNumber;
        return this;
    }


    public TaxiDriverParam drivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
        return this;
    }

    public TaxiDriverParam emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public TaxiDriverParam firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TaxiDriverParam lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TaxiDriverParam nicn(String nicn) {
        this.nicn = nicn;
        return this;
    }

    public TaxiDriverParam placeOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public TaxiDriverParam telephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }


    public TaxiDriverParam nationalite(String nationalite) {
        this.nationalite = nationalite;
        return this;
    }

    public TaxiDriverParam departementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
        return this;
    }

    public TaxiDriverParam situationMatrimoniale(SituationMatrimonialeEnum situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
        return this;
    }

    public TaxiDriverParam domicile(String domicile) {
        this.domicile = domicile;
        return this;
    }

    public TaxiDriverParam telUrgent(String telUrgent) {
        this.telUrgent = telUrgent;
        return this;
    }

    public TaxiDriverParam numRecepisse(String numRecepisse) {
        this.numRecepisse = numRecepisse;
        return this;
    }



    public TaxiDriverParam situation(SituationEnum situation) {
        this.situation = situation;
        return this;
    }

    public TaxiDriverParam cityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public TaxiDriverParam organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }

    public LocalDate getCapacityExpiredDate() {
        return capacityExpiredDate;
    }

    public void setCapacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
    }

    public LocalDate getDrivingLicenseExpiredDate() {
        return drivingLicenseExpiredDate;
    }

    public void setDrivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
    }

    public LocalDate getNicnExpiredDate() {
        return nicnExpiredDate;
    }

    public void setNicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
    }

    public TaxiDriverParam capacityExpiredDate(LocalDate capacityExpiredDate) {
        this.capacityExpiredDate = capacityExpiredDate;
        return this;
    }

    public TaxiDriverParam drivingLicenseExpiredDate(LocalDate drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
        return this;
    }

    public TaxiDriverParam nicnExpiredDate(LocalDate nicnExpiredDate) {
        this.nicnExpiredDate = nicnExpiredDate;
        return this;
    }

    public LocalDate getValidityBadgeDate() {
        return validityBadgeDate;
    }

    public void setValidityBadgeDate(LocalDate validityBadgeDate) {
        this.validityBadgeDate = validityBadgeDate;
    }

    public String getTelephoneNumberBis() {
        return telephoneNumberBis;
    }

    public void setTelephoneNumberBis(String telephoneNumberBis) {
        this.telephoneNumberBis = telephoneNumberBis;
    }

    public String getLieuNicn() {
        return lieuNicn;
    }

    public void setLieuNicn(String lieuNicn) {
        this.lieuNicn = lieuNicn;
    }

    public TimbreFiscalEnum getTimbreFiscal() {
        return timbreFiscal;
    }

    public void setTimbreFiscal(TimbreFiscalEnum timbreFiscal) {
        this.timbreFiscal = timbreFiscal;
    }

    public TaxiDriverParam validityBadgeDate(LocalDate validityBadgeDate) {
        this.validityBadgeDate = validityBadgeDate;
        return this;
    }

    public TaxiDriverParam telephoneNumberBis(String telephoneNumberBis) {
        this.telephoneNumberBis = telephoneNumberBis;
        return this;
    }

    public TaxiDriverParam lieuNicn(String lieuNicn) {
        this.lieuNicn = lieuNicn;
        return this;
    }

    public TaxiDriverParam timbreFiscal(TimbreFiscalEnum timbreFiscal) {
        this.timbreFiscal = timbreFiscal;
        return this;
    }
}
