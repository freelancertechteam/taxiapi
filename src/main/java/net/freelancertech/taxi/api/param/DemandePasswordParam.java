package net.freelancertech.taxi.api.param;

import net.freelancertech.taxi.api.typage.ModeReceiveEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by simon on 19/01/2017.
 */
public class DemandePasswordParam {

    @NotNull
    @NotEmpty
    private String pointContact;
    @NotNull
    private ModeReceiveEnum modeReceive;

    public String getPointContact() {
        return pointContact;
    }

    public void setPointContact(String pointContact) {
        this.pointContact = pointContact;
    }

    public ModeReceiveEnum getModeReceive() {
        return modeReceive;
    }

    public void setModeReceive(ModeReceiveEnum modeReceive) {
        this.modeReceive = modeReceive;
    }

    public DemandePasswordParam pointContact(String pointContact) {
        this.pointContact = pointContact;
        return this;
    }

    public DemandePasswordParam modeReceive(ModeReceiveEnum modeReceive) {
        this.modeReceive = modeReceive;
        return this;
    }
}
