package net.freelancertech.taxi.api.param;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Organisation.
 */

public class ContactParam implements Serializable {

    private static final long serialVersionUID = 1L;

    private String telephone;

    private String emailAddress;


    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactParam)) return false;
        ContactParam that = (ContactParam) o;
        return Objects.equals(getTelephone(), that.getTelephone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTelephone());
    }

    public ContactParam telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public ContactParam emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

}
