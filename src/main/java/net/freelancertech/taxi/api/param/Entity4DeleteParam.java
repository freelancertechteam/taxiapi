package net.freelancertech.taxi.api.param;


/**
 * Created by SimonPascal on 08/08/2015.
 */

public class Entity4DeleteParam {

    private String code;
    private int version;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


    public Entity4DeleteParam code(String code) {
        this.code = code;
        return this;
    }

    public Entity4DeleteParam version(int version) {
        this.version = version;
        return this;
    }
}
