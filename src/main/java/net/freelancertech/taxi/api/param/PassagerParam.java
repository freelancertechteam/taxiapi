package net.freelancertech.taxi.api.param;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;


/**
 * A DTO for the Passager entity.
 */
public class PassagerParam implements Serializable {


    @NotNull
    @NotEmpty
    @Size(max = 35)
    private String phone;

    @NotNull
    private String nomPrenom;

    @NotNull
    private String lieuArrive;

    private String lieuDepart;

    private BigDecimal latitude;

    private BigDecimal longitude;

    private String divers;

    private String phoneImei;

    private String phoneSerialNumber;


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getNomPrenom() {
        return nomPrenom;
    }

    public void setNomPrenom(String nomPrenom) {
        this.nomPrenom = nomPrenom;
    }
    public String getLieuArrive() {
        return lieuArrive;
    }

    public void setLieuArrive(String lieuArrive) {
        this.lieuArrive = lieuArrive;
    }
    public String getLieuDepart() {
        return lieuDepart;
    }

    public void setLieuDepart(String lieuDepart) {
        this.lieuDepart = lieuDepart;
    }
    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    public String getDivers() {
        return divers;
    }

    public void setDivers(String divers) {
        this.divers = divers;
    }
    public String getPhoneImei() {
        return phoneImei;
    }

    public void setPhoneImei(String phoneImei) {
        this.phoneImei = phoneImei;
    }
    public String getPhoneSerialNumber() {
        return phoneSerialNumber;
    }

    public void setPhoneSerialNumber(String phoneSerialNumber) {
        this.phoneSerialNumber = phoneSerialNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PassagerParam)) return false;
        PassagerParam that = (PassagerParam) o;
        return Objects.equals(getPhone(), that.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPhone());
    }

    @Override
    public String toString() {
        return "PassagerDTO{" +
            ", phone='" + phone + "'" +
            ", nomPrenom='" + nomPrenom + "'" +
            ", lieuArrive='" + lieuArrive + "'" +
            ", lieuDepart='" + lieuDepart + "'" +
            ", latitude='" + latitude + "'" +
            ", longitude='" + longitude + "'" +
            ", divers='" + divers + "'" +
            ", phoneImei='" + phoneImei + "'" +
            ", phoneSerialNumber='" + phoneSerialNumber + "'" +
            '}';
    }


    public PassagerParam phone(String phone) {
        this.phone = phone;
        return this;
    }

    public PassagerParam nomPrenom(String nomPrenom) {
        this.nomPrenom = nomPrenom;
        return this;
    }

    public PassagerParam lieuArrive(String lieuArrive) {
        this.lieuArrive = lieuArrive;
        return this;
    }

    public PassagerParam lieuDepart(String lieuDepart) {
        this.lieuDepart = lieuDepart;
        return this;
    }

    public PassagerParam latitude(BigDecimal latitude) {
        this.latitude = latitude;
        return this;
    }

    public PassagerParam longitude(BigDecimal longitude) {
        this.longitude = longitude;
        return this;
    }

    public PassagerParam divers(String divers) {
        this.divers = divers;
        return this;
    }

    public PassagerParam phoneImei(String phoneImei) {
        this.phoneImei = phoneImei;
        return this;
    }

    public PassagerParam phoneSerialNumber(String phoneSerialNumber) {
        this.phoneSerialNumber = phoneSerialNumber;
        return this;
    }

}
