package net.freelancertech.taxi.api.param;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


/**
 * A DTO for the City entity.
 */
public class CityToUpdatePARAM implements Serializable {
    @NotNull
    @Size(max = 10)
    private String code;
    @NotNull
    @Size(max = 60)
    private String name;
    @NotNull
    private Integer version;

    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CityToUpdatePARAM name(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CityToUpdatePARAM description(String description) {
        this.description = description;
        return this;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public CityToUpdatePARAM version(Integer version) {
        this.version = version;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CityToUpdatePARAM code(String code) {
        this.code = code;
        return this;
    }
}
