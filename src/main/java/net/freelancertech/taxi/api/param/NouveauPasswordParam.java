package net.freelancertech.taxi.api.param;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by simon on 19/01/2017.
 */
public class NouveauPasswordParam {

    @NotNull
    @NotEmpty
    private String password;
    @NotNull
    @NotEmpty
    private String code;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public NouveauPasswordParam password(String password) {
        this.password = password;
        return this;
    }

    public NouveauPasswordParam code(String code) {
        this.code = code;
        return this;
    }
}
