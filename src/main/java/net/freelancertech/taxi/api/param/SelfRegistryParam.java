package net.freelancertech.taxi.api.param;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Organisation.
 */

public class SelfRegistryParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @NotEmpty
    private String telephone;
    @NotEmpty
    private String emailAddress;

    @NotNull
    @NotEmpty
    private String organisationCode;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getOrganisationCode() {
        return organisationCode;
    }

    public void setOrganisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SelfRegistryParam)) return false;
        SelfRegistryParam that = (SelfRegistryParam) o;
        return Objects.equals(getTelephone(), that.getTelephone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTelephone());
    }

    public SelfRegistryParam telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public SelfRegistryParam emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public SelfRegistryParam organisationCode(String organisationCode) {
        this.organisationCode = organisationCode;
        return this;
    }
}
