package net.freelancertech.taxi.api.param;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;


/**
 * A DTO for the PassagerCoordonnee entity.
 */
public class PassagerCoordonneeParam implements Serializable {


    @NotNull
    private BigDecimal latitude;

    @NotNull
    private BigDecimal longitude;


    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }




    @Override
    public String toString() {
        return "PassagerCoordonneeDTO{" +
            ", latitude='" + latitude + "'" +
            ", longitude='" + longitude + "'" +
            '}';
    }


    public PassagerCoordonneeParam latitude(BigDecimal latitude) {
        this.latitude = latitude;
        return this;
    }

    public PassagerCoordonneeParam longitude(BigDecimal longitude) {
        this.longitude = longitude;
        return this;
    }
}
