package net.freelancertech.taxi.utils;
/**
 * Created by simon on 11/05/2017.
 */
public class HeaderKeyUtils {
    public static final String TAXI_ERROR_KEY="X-taxiApp-error";
    public static final String TAXI_ALERT_KEY="X-taxiApp-alert";

    private HeaderKeyUtils() {
    }


}
