package net.freelancertech.taxi.dto.gouvernement;

import net.freelancertech.taxi.dto.AbstractStatsDto;

/**
 * Created by simon on 17/05/2017.
 */
public class AgentPublicStatsDto extends AbstractStatsDto {
    /**
     * nombre membres d'un syndicat.
     */
    private int effectif;
    /**
     * nombre de cotisation.
     */
    private int effectifCotisation;
    /**
     * nombre de piece cni en cours d'expiration.
     */
    private int nombreExpirationCni;
    /**
     * nombre de piece capacite en cours d'expiration.
     */
    private int nombreExpirationCapacite;
    /**
     * nombre de piece permis de conduire en cours d'expiration.
     */
    private int nombreExpirationPermisConduire;


    public int getEffectif() {
        return effectif;
    }

    public void setEffectif(int effectif) {
        this.effectif = effectif;
    }

    public int getEffectifCotisation() {
        return effectifCotisation;
    }

    public void setEffectifCotisation(int effectifCotisation) {
        this.effectifCotisation = effectifCotisation;
    }

    public int getNombreExpirationCni() {
        return nombreExpirationCni;
    }

    public void setNombreExpirationCni(int nombreExpirationCni) {
        this.nombreExpirationCni = nombreExpirationCni;
    }

    public int getNombreExpirationCapacite() {
        return nombreExpirationCapacite;
    }

    public void setNombreExpirationCapacite(int nombreExpirationCapacite) {
        this.nombreExpirationCapacite = nombreExpirationCapacite;
    }

    public int getNombreExpirationPermisConduire() {
        return nombreExpirationPermisConduire;
    }

    public void setNombreExpirationPermisConduire(int nombreExpirationPermisConduire) {
        this.nombreExpirationPermisConduire = nombreExpirationPermisConduire;
    }

    public AgentPublicStatsDto effectif(int effectif) {
        this.effectif = effectif;
        return this;
    }

    public AgentPublicStatsDto effectifCotisation(int effectifCotisation) {
        this.effectifCotisation = effectifCotisation;
        return this;
    }

    public AgentPublicStatsDto nombreExpirationCni(int nombreExpirationCni) {
        this.nombreExpirationCni = nombreExpirationCni;
        return this;
    }

    public AgentPublicStatsDto nombreExpirationCapacite(int nombreExpirationCapacite) {
        this.nombreExpirationCapacite = nombreExpirationCapacite;
        return this;
    }

    public AgentPublicStatsDto nombreExpirationPermisConduire(int nombreExpirationPermisConduire) {
        this.nombreExpirationPermisConduire = nombreExpirationPermisConduire;
        return this;
    }
}
