package net.freelancertech.taxi.dto.syndicat;

import java.time.LocalDate;
import java.util.Date;

/**
 * Created by simon on 17/05/2017.
 */
public class MembreExpirationDto {
    private String matricule;
    private String firstName;
    private String lastName;
    private String contact;
    private String contactUrgence;
    private LocalDate expirationDate;
    private String pieceCode;

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactUrgence() {
        return contactUrgence;
    }

    public void setContactUrgence(String contactUrgence) {
        this.contactUrgence = contactUrgence;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPieceCode() {
        return pieceCode;
    }

    public void setPieceCode(String pieceCode) {
        this.pieceCode = pieceCode;
    }

    public MembreExpirationDto matricule(String matricule) {
        this.matricule = matricule;
        return this;
    }

    public MembreExpirationDto firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public MembreExpirationDto lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public MembreExpirationDto contact(String contact) {
        this.contact = contact;
        return this;
    }

    public MembreExpirationDto contactUrgence(String contactUrgence) {
        this.contactUrgence = contactUrgence;
        return this;
    }

    public MembreExpirationDto expirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public MembreExpirationDto pieceCode(String pieceCode) {
        this.pieceCode = pieceCode;
        return this;
    }
}
