package net.freelancertech.taxi.dto.syndicat;

/**
 * Created by simon on 17/05/2017.
 */
public class SyndicatCotisationDto {
    /**
     * periode année, mois ou jour
     */
    private int periode;
    /**
     * nombre d'element
     */
    private long effectif;

    public int getPeriode() {
        return periode;
    }

    public void setPeriode(int periode) {
        this.periode = periode;
    }

    public long getEffectif() {
        return effectif;
    }

    public void setEffectif(long effectif) {
        this.effectif = effectif;
    }

    public SyndicatCotisationDto periode(int periode) {
        this.periode = periode;
        return this;
    }

    public SyndicatCotisationDto effectif(long effectif) {
        this.effectif = effectif;
        return this;
    }
}
