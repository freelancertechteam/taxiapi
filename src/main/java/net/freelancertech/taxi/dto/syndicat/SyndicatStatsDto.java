package net.freelancertech.taxi.dto.syndicat;

import net.freelancertech.taxi.dto.AbstractStatsDto;

/**
 * resortir les statistics
 * Created by simon on 17/05/2017.
 */
public class SyndicatStatsDto extends AbstractStatsDto {
    /**
     * nombre membres d'un syndicat.
     */
    private long effectif;
    /**
     * nombre de cotisation.
     */
    private long effectifCotisation;
    /**
     * nombre de piece cni en cours d'expiration.
     */
    private long nombreExpirationCni;
    /**
     * nombre de piece capacite en cours d'expiration.
     */
    private long nombreExpirationCapacite;
    /**
     * nombre de piece permis de conduire en cours d'expiration.
     */
    private long nombreExpirationPermisConduire;

    /**
     * nombre de piece permis de conduire en cours d'expiration.
     */
    private long nombreMembreInfoNotComplete;

    /**
     * nombre de recepisse dont la date de delivrance est atteinte ou proche.
     */
    private long nombreRecepisseDelivrance;
    /**
     * nombre de recepisse dont la periode de validite atteinte ou proche.
     */
    private long nombreRecepissePeriodeValidite;




    public long getEffectif() {
        return effectif;
    }

    public void setEffectif(long effectif) {
        this.effectif = effectif;
    }

    public long getEffectifCotisation() {
        return effectifCotisation;
    }

    public void setEffectifCotisation(long effectifCotisation) {
        this.effectifCotisation = effectifCotisation;
    }

    public long getNombreExpirationCni() {
        return nombreExpirationCni;
    }

    public void setNombreExpirationCni(long nombreExpirationCni) {
        this.nombreExpirationCni = nombreExpirationCni;
    }

    public long getNombreExpirationCapacite() {
        return nombreExpirationCapacite;
    }

    public void setNombreExpirationCapacite(long nombreExpirationCapacite) {
        this.nombreExpirationCapacite = nombreExpirationCapacite;
    }

    public long getNombreExpirationPermisConduire() {
        return nombreExpirationPermisConduire;
    }

    public void setNombreExpirationPermisConduire(long nombreExpirationPermisConduire) {
        this.nombreExpirationPermisConduire = nombreExpirationPermisConduire;
    }

    public long getNombreRecepisseDelivrance() {
        return nombreRecepisseDelivrance;
    }

    public void setNombreRecepisseDelivrance(long nombreRecepisseDelivrance) {
        this.nombreRecepisseDelivrance = nombreRecepisseDelivrance;
    }

    public long getNombreRecepissePeriodeValidite() {
        return nombreRecepissePeriodeValidite;
    }

    public void setNombreRecepissePeriodeValidite(long nombreRecepissePeriodeValidite) {
        this.nombreRecepissePeriodeValidite = nombreRecepissePeriodeValidite;
    }

    public SyndicatStatsDto effectif(long effectif) {
        this.effectif = effectif;
        return this;
    }

    public SyndicatStatsDto effectifCotisation(long effectifCotisation) {
        this.effectifCotisation = effectifCotisation;
        return this;
    }

    public SyndicatStatsDto nombreExpirationCni(long nombreExpirationCni) {
        this.nombreExpirationCni = nombreExpirationCni;
        return this;
    }

    public SyndicatStatsDto nombreExpirationCapacite(long nombreExpirationCapacite) {
        this.nombreExpirationCapacite = nombreExpirationCapacite;
        return this;
    }

    public SyndicatStatsDto nombreExpirationPermisConduire(long nombreExpirationPermisConduire) {
        this.nombreExpirationPermisConduire = nombreExpirationPermisConduire;
        return this;
    }

    public long getNombreMembreInfoNotComplete() {
        return nombreMembreInfoNotComplete;
    }

    public void setNombreMembreInfoNotComplete(long nombreMembreInfoNotComplete) {
        this.nombreMembreInfoNotComplete = nombreMembreInfoNotComplete;
    }

    public SyndicatStatsDto nombreMembreInfoNotComplete(long nombreMembreInfoNotComplete) {
        this.nombreMembreInfoNotComplete = nombreMembreInfoNotComplete;
        return this;
    }

    public SyndicatStatsDto nombreRecepisseDelivrance(long nombreRecepisseDelivrance) {
        this.nombreRecepisseDelivrance = nombreRecepisseDelivrance;
        return this;
    }

    public SyndicatStatsDto nombreRecepissePeriodeValidite(long nombreRecepissePeriodeValidite) {
        this.nombreRecepissePeriodeValidite = nombreRecepissePeriodeValidite;
        return this;
    }
}
